package com.techu.apitechu;

import com.techu.apitechu.Models.ProductModels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechuApplication {

	public static ArrayList <com.techu.apitechu.Models.ProductModels> ProductModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);

		ApitechuApplication.ProductModels = ApitechuApplication.getTestDate();
	}

	private static ArrayList <ProductModels> getTestDate() {

		ArrayList <ProductModels> ProductModels= new ArrayList<>();

		ProductModels.add(
			new ProductModels(
				"1",
				"Producto 1",
				10
	)
			);
		ProductModels.add(
				new ProductModels(
			"2",
			"Producto 2",
			20
	)
			);
			ProductModels.add(
					new ProductModels(
			"3",
			"Producto 3",
			30
	)
			);

			return ProductModels;
	}
}
