package com.techu.apitechu.Controller;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.Models.ProductModels;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductControler {
    // CRUD
    //HEAD OPTION
    static final String APIBaseUrl = "/apitechu/vl";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModels> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.ProductModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModels getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("la id del producto a otener es  " + id);

        ProductModels result = new ProductModels();

        for (ProductModels product : ApitechuApplication.ProductModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }
        }
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModels createProduct(@RequestBody ProductModels newProduct) {
        System.out.println("Create Product");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("La descripcion del producto es " + newProduct.getDesc());
        System.out.println("El precio del producto es " + newProduct.getPrice());

        ApitechuApplication.ProductModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModels updateProduct(@RequestBody ProductModels product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en param URL " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModels productIntList : ApitechuApplication.ProductModels) {
            if (productIntList.getId().equals(id)) {
                productIntList.setId(product.getId());
                productIntList.setDesc(product.getDesc());
                productIntList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @PutMapping(APIBaseUrl + "/products/par/{id}")
    public ProductModels updateProductPar(@RequestBody ProductModels productPar, @PathVariable String id) {
        System.out.println("updateProductPar");
        System.out.println("La id del producto a actualizar parcialmente es " + productPar.getId());
        System.out.println("La descripcion del producto a actualizar es " + productPar.getDesc());
        System.out.println("El precio del producto a actualizar es " + productPar.getPrice());

        ProductModels result = new ProductModels();

        for (ProductModels productIntList : ApitechuApplication.ProductModels) {
            if (productIntList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = productIntList;

                if (productPar.getDesc() != null) {
                    System.out.println("Actualizando descripcion del producto");
                    productIntList.setDesc(productPar.getDesc());
                }
                if (productPar.getPrice() > 0) {
                    System.out.println("Actualizado el precio del producto");
                    productIntList.setPrice(productPar.getPrice());
                }
            }
        }
        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
        public ProductModels deleteProduct (@PathVariable String id){
            System.out.println("deleteProduct");
            System.out.println("La id del producto a borrar es: " + id);

            ProductModels result= new ProductModels();
            boolean foundCompany = false;

            for (ProductModels productIntlist : ApitechuApplication.ProductModels) {
                if (productIntlist.getId().equals(id)){
                    System.out.println("Producto encontrado");
                    foundCompany = true;
                    result = productIntlist;
            }
        }
        if (foundCompany == true){
            System.out.println("Borrando producto");
            ApitechuApplication.ProductModels.remove(result);
        }
        return new ProductModels();
    }

}
